For use by the People Group for general issues, queries and problems we can help solve. This issue tracker is also used by Team members for issues or suggestions in general to the People Group. We have dedicated issue trackers for training, hiring, onboarding, or offboarding, we therefore do not use this issue tracker for those items. 
Please assign this issue or tag the necessary team within the People Success team, to ensure it is prioritised and viewed. [Find more info in the handbook on how to find the right team in the People Group](https://about.gitlab.com/handbook/people-group/#how-to-reach-the-right-member-of-the-people-group).
We do have a template in our issue tracker that defaults to a few key things we need to take the best action and assign it to the right person. If the template does not relate to what you want to say/ do, feel free to just delete the contents of the new issue and add the label: workflow::triage. 

## For People Operations
### Labels 

Labels we use to build our board (our Prioritized labels) are as follow:
- Workflow::triage (new issue and no triaging has been done on the issue)
- Workflow::waiting (we worked on the issue and waiting for a response or another issue or MR to unblock us)
- Workflow::Todo (adding to the backlog of work todo and add a due date )
- Workflow::doing (we are working on this now, towards our due dae)
- Workflow::Blocked  These issues are either blocked by another issue. The People Ops Team always adds the reason why an issue is moved to blocked.

### Priority Labels

| Priority Level | Compliance | Impact                                        | Projects                   |
|----------------|------------|-----------------------------------------------|----------------------------|
| p1 (~pops::p1) | yes        | Any                                           | Contracts                  |
| p1 (~pops::p1) | no         | OKR progression                               | Workday, Engagement Survey |
| p2 (~pops::p2) | no         | Company or Globally                           |                            |
| p3 (~pops::p3) | no         | Division, Department, Team, Country Specific  |                            |
| p4 (~pops::p4) | no         | Any                                           |                            |
| p5 (~pops::p5) | no         | Any                                           |                            |



## For the People Business Partner (PBP) 
### Labels 
Team we are using the following labels to divide and track work: 
* pbp::attention: This label should be added by any team member who requires People Business Partner attention/review/feedback on an issue

When collaborating on the issues PBP's will work with the following labels: 
* pbp::DIB: This label should be added by a People Business Partner to indicate collaboration on a Diversity, Inclusion, & Belonging issue
* pbp::LD: This label should be added by a People Business Partner to indicate collaboration on a Learning & Development issue
* pbp::TR: This label should be added by a People Business Partner to indicate collaboration on a Total Rewards issue
* pbp::legal: This label should be added by a People Business Partner to indicate collaboration on a Legal issue
* pbp::peopleops: This label should be added by a People Business Partner to indicate collaboration on a People Ops issue
* pbp::recruiting: This label should be added by a People Business Partner to indicate collaboration on a Recruiting issue 
* pbp::other: This label should be added by a People Business Partner to indicate collaboration with another team (outside of Total Rewards, L&D, DIB, Recruiting, People Ops, or Legal)


### Priority Labels

Furthermore PBPs indicate priority by these labels: 
* pbp::high: This label should be added by a People Business Partner during triaging to indicate "urgent" priority
* pbp::medium: This label should be added by a People Business Partner during triaging to indicate "medium" priority
* pbp::low: This label should be added by a People Business Partner during triaging to indicate "low" priority

Once issues/MRs are reviewed/worked on with t he PBPs we'll assign the label: 
* pbp::done: This label should be added by a member of the People Business Partner team to indicate that the issue has been reviewed

For the People Experience Team (PEA) we are using the following labels to track out work:
* PEA Team: to lable with any issues opened that need attention from the PEA team

* PEA::Waiting: to move any new issues into when first opened or reviewed
* PEA::ToDo: to move any issues that need to have action taken on them
* PEA::Doing: issues that you are actively workin on
* PEA::Closed: issues that have been completed

 
 


 
 
