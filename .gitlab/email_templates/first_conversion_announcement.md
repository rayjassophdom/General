Hi All,

I hope this email finds you well!

We are currently considering a new employment model in {{Country}} that can support GitLab in scaling and ensuring a great people experience. The logic behind this consideration is to minimize risk for both GitLab and our team members.

We are still in the early stages, so we have few details to share at this time, but we want to make sure you are in the loop during the evaluation process. All updates will be made real-time in this issue: {{INSERT_ISSUE_LINK}} so you can stay up to date as we move through the process. Meanwhile, we would like to share a few helpful resources and set expectations in regard to the process early:

* Our [International Expansion handbook page](https://about.gitlab.com/handbook/people-group/contracts-and-international-expansion/#international-expansion) for your review, which outlines the process and steps taken during country conversions.
* We encourage you to watch the [Country Conversions and International Expansion GitLab Unfiltered Interview](https://www.youtube.com/watch?v=3Eoo4QJXr4E&feature=youtu.be), which answers some of the most common questions that tend to arise during this process. 
* The [Compensation Review process](https://about.gitlab.com/handbook/people-group/contracts-and-international-expansion/#compensation-review-for-country-conversions) for country conversions for your reference. 
* The [high level timeline](https://about.gitlab.com/handbook/people-group/contracts-and-international-expansion/#country-conversions-workflow) for the country conversions process. _Please note that the research phase can take longer than outlined depending on the complexity of the conversion and other variables. We will keep all team members updated in the country issue.
* There will be an AMA arranged prior to finalizing the conversion details to ensure you have a chance to address all of your questions. 
* You will receive a gross total reward package overview prior to finalizing the conversion. 
* You will have up to 2 weeks to review contracts prior to the conversion date. 

We value team member insight and suggestions immensely! You are very much encouraged to add feedback directly in the issue.

If you have any questions or concerns please contact the Country Conversions team at countryconversions@gitlab.com.

Thank you,
