Subject: [`Natural Disaster`] in [`Location`] - Team Member Check In
(Remember to cc `peopleops@gitlab.com`, `peoplepartners@gitlab.com` and `people-exp@gitlab.com`)

Hello Fellow Team Members,

We have been notified of the [earthquakes](insert new report link here) which took place in {Location} and wanted to conduct a check-in to ensure that all team members within the region along with their families and loved ones are safe.

Kindly note that your safety is of the utmost importance to GitLab and we ask that when possible you respond to this email thread or communicate with us via Slack to confirm that you are in a secure location.

Additionally we encourage all team members to adhere to government guidelines around evacuations and carry out any precautions / preparations that may be necessary at this time.  Please also be sure to review the [Disaster Recovery Page](https://about.gitlab.com/handbook/people-group/disaster-recovery-plan/) within the handbook which provides information and resources to support you.

As these events are unexpected it can be difficult to decide on an immediate course of action, we encourage you to:

- Know your zone i.e. familiarise yourself with your community evacuation zones and ensure you know where to head when the time comes.  If a natural disaster threatens your community and local officials initiate evacuations don't hesitate.
- Have an [emergency family communication plan](https://www.ready.gov/plan) in place identifying suitable ways to maintain contact throughout the event - this includes identifying suitable locations to reconnect at a later stage should you be separated.
- Make use of the {`Get Ready Emergency Application`} (insert a link to local application) which includes real time information surrounding the natural event and offers various resources to guide your preparations.
- Lean into the various resources offered by GitLab including reimbursement for ground transport utilised in the evacuation process and the [Modern Health Employee Assistance Program](https://about.gitlab.com/handbook/total-rewards/benefits/modern-health/).

Should we not hear from you within 24 hours we will extend this communication to your personal email address after which we will try to establish contact with your next of kin.

Along with this we will also reach out to all Managers and ensure they are aware of the situation and that you may be unavailable for an undisclosed period.

Please be sure to reach out to the People Experience Team, a GitLab People Business Partner or your Manager should you require any additional support or information.

