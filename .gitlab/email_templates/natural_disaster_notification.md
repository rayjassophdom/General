Subject: [`Natural Disaster`] in [`Location`]   
(Remember to cc `peopleops@gitlab.com` and `team member's manager`)

Hello `Team Member Name`,

We have been notified about the [`Natural Disaster`] in your area and we want to make sure that all GitLab team members and their families are safe.  Please take the appropriate steps to ensure you and your family’s safety.  

As the [`Natural Disaster`] continues or worsens please Slack or email if and when possible, by replying to all on this thread. This check is only to let us know that you are in a safe environment.  Your safety is our number one priority; please adhere to your local government warnings regarding evacuations and all safety warnings! 

As the storm approaches, it is often too late to get ready. Make sure you: 

• Know your zone. Evacuations are more common than people realize. Make yourself familiar with your community’s evacuation zones, so you’ll know exactly where to go. **Remember: if a [`Natural Disaster`] threatens your community and local officials say it's time to evacuate, don't hesitate -- go early.**

• Complete a family communication plan. Plan how you will assemble your family and loved ones, and anticipate where you will go for different situations. Get together with your family and agree on the ways to contact one another in an emergency, identify meeting locations, and make a [Family Emergency Communication Plan](https://www.ready.gov/plan). 

• Download the [`Emergency App in region (if applicable)`]. For example, the [FEMA app](https://www.fema.gov/mobile-app) includes disaster resources, weather alerts, safety tips, and a new feature that will enable users to receive push notifications to their devices to prepare their homes and families for disasters. The app also provides a customizable checklist of emergency supplies, maps of open shelters and recovery centers, tips on how to survive disasters, and weather alerts from the National Weather Service. 

• Review the [Disaster Recovery Plan Handbook](https://about.gitlab.com/handbook/people-group/#disaster-recovery-plan) page, if you have any questions. 

Again, we urge all GitLab team members to follow their local authorities warnings.  Please do not hesitate to reach out to us or to your GitLab People Business Partner or Manager at any time if there is an issue.

Sincerely,
[`People Operations Specialist Name`] 
