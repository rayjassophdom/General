Subject: [Team Member Full Name] - Probation Period Notification 
To: [Manager GitLab Email], cc `people-exp@domain.com`)

Dear [Manager First Name],

This message is to notify you that [Team Member]'s probation period is ended on [Probation Period End Date]. Unfortunately we did not contact you to confirm whether you were satisfied with their performance prior to the end of their probation period, which is part of our current and ongoing process. This message will be saved in [Team Member]'s BambooHR profile. 

More info on our probation periods can be found on this page: https://about.gitlab.com/handbook/people-group/contracts-and-international-expansion/#probation-period

Please let me know if you have any questions.

Thank you,
[People Experience Associate]