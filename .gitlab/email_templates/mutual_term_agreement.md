Hi Team!

Now that your contracts with {{COUNTRY_VENDOR}} are signed and in place, we will be sending Mutual Termination Agreements to formally end your contracts with {{CURRENT_EMPLOYER}}. 

I'll be preparing these and sending them for staging within the {{TIMELINE_TO_SEND_MTAS}}, just wanted to ensure a heads up with context was provided. 

Feel free to reach out with any questions!

Best,