# Exit Interview Template 

I wanted to schedule time with you to go over the exit interview questions that were sent to you via Culture Amp.  This meeting is optional, if there is additional information that you would like to share please accept this invite or propose a new time. If you do not wish to have an in person exit interview you can simply decline the meeting.Thanks and I wish you the best in your future endeavors!
