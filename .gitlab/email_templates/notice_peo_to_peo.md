Hi {VendorName},

I hope this email finds you well. 

While we are appreciative of the partnership between GitLab and {VendorName} over the past {TimeInPartnership}, after thorough analysis we have decided to move forward with a different PEO vendor in {Country}.

{IncludeSomeReasoning}

Please consider this email as an official notice to terminate our contract. As the notice period agreed upon is {NoticePeriod}, we are looking to transfer our team by {DateOfTransition}; the official transfer date is to be determined, we will certainly keep you in the loop. 

Please let us know if anything else is required from our end. 

Best regards,
{People Operations Specialist}
