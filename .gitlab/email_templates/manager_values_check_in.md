Hi `__FIRST_OR_PREFERRED_NAME__`,

We complete a <a href="https://about.gitlab.com/handbook/people-group/values-check-in">values check-in</a> with all new hires on day 90, `__FULL_NAME__` in your team has now reached their 90th day! Read through the page and have a look at what team members are asked during the survey we send.

Please add a note to your next 1:1 to check-in with the team member and ask how the first 90 days have been.

This is not a performance check-in, any <a href="https://about.gitlab.com/handbook/people-group/leadership-toolkit/career-development-conversations/">career development</a> or <a href="https://about.gitlab.com/handbook/underperformance/">underperformance</a> discussions should be coordinated by the manager of the team member directly.

Feel free to contact the People Specialist team (peopleops@gitlab.com) if you have any questions or concerns.

Best regards,

People Experience Associate Team
