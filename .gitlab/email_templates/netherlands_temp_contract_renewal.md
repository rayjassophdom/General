Hello `__MANAGER_NAME__`,

I hope this message finds you well.

As you may or may not know, your direct report `__TEAM_MEMBER_NAME__` is nearing the end of their first 12-month GitLab BV Temporary Contract, which is common for Dutch employers. `__TEAM_MEMBER_NAME__`'s start date was `__TEAM_MEMBER_START_DATE__`.

As it is less than two months before their contract ends, it is part of my duty as a People Experience Associate to notify you that you have a few options. Please take the next two weeks to evaluate the performance of this team member and discuss the planned course of action with your People Business Partner, cc'd on this email. They are a wonderful resource to provide guidance on this process and the best way to communicate your decision.

The two main options are, per the <a href='https://about.gitlab.com/handbook/people-group/contracts-probation-periods/#netherlands-renewal-process'>handbook section</a> for this process:

1. If you are satisfied with your team member and would like to extend their employment with a second 12-month Temporary Contract, please notify the team member in question, as well as myself by the deadline of `__DUE_DATE__` so that I may create, stage and send them their new contract. Per the handbook, it is GitLab's practice and policy that all Netherlands team members, should they successfully complete their first 12-month Temporary Contract, be given a second 12-month Temporary Contract. (At the end of their second 12-month contract, if they are in good standing, they are given an Indefinite Agreement.)

2. If you are not satisfied with your team member and do not want to move forward to retain them, please discuss with your PBP on the best way to proceed. I will be notified by the PBP if this is the case, and support as needed. The Netherlands requires a 1-month notice period, so this decision must be made and communicated to the team member before `__DUE_DATE__`.

Please let us know if you have any questions on this process.

Thank you!
