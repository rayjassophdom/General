Hi `__FIRST_OR_PREFERRED_NAME__`,

GitLab's values are a crucial part of our competencies list and are considered essential for team members to learn and live out, irrespective of role or team. To ensure all team members are living out and practicing GitLab's values, we have implemented a <a href="https://docs.google.com/forms/d/e/1FAIpQLSd71MxvRaBjhaxSiFW0qo0blULu9jQ0ypkU7zPEU3p-IimpIQ/viewform">Values Check-In Survey</a> for new team members. The purpose of the check-in is to ensure alignment from a performance perspective and understanding of expectations with regard to the values. 

GitLab's values check-in process is to check in with new hires on day 90. This is to ensure that we check in with team members, make sure they are supported, get feedback on their first 90 days at GitLab and iterate on valuable feedback. This is not a performance check-in, any <a href="https://about.gitlab.com/handbook/people-group/leadership-toolkit/career-development-conversations/">career development</a> or <a href="https://about.gitlab.com/handbook/underperformance/">underperformance</a> discussions should be coordinated by the manager of the team member directly.

The purpose of this survey is to facilitate an opportunity for you and your manager to reflect on your alignment with Gitlab and GitLab's values thus far. We recommend you read through our <a href='https://about.gitlab.com/handbook/values/'>values handbook page</a> a second time prior to taking this survey.

We encourage you to also complete our <a href='https://about.gitlab.com/handbook/people-group/learning-and-development/certifications/#current-knowledge-assessments'>Values Knowledge Assessments</a>, if you have not done so already.

Please complete this survey and ensure you connect live with your manager to discuss by `__DEADLINE__`.

Feel free to contact the People Experience team (people-exp@gitlab.com) if you have any questions or concerns.

Best,

People Experience Associate Team
