Subject: GitLab Offboarding Information

To: `__DEPARTING_TEAM_MEMBER__` using their GitLab email and Personal email

Hello `__DEPARTING_TEAM_MEMBER__`

We were recently informed that you are leaving GitLab and wanted to send you important offboarding information. This can be found in our [GitLab Offboarding FAQ page](https://about.gitlab.com/handbook/people-group/offboarding/faq.html). Feel free to reach out should you have any questions.

*Please be sure that we have a personal email address for you in BambooHR, as various teams will be communicating with you throughout the offboarding process using this address.*

**When will you receive your final pay?**

Your final check (directly deposited) will be paid this Friday or next Friday depending on [payroll's schedule](https://about.gitlab.com/handbook/finance/payroll/#pay-date), for the current Pay Period, and include all days of pay from the last pay period to your last day of work. For this exact information, feel free to email `nonuspayroll@gitlab.com`.

**When will you receive (if applicable) commissions payment(s)?**

Any final commissions earned up until your last working day will be paid (directly deposited) at the end of the next month. Commissions are paid one month in arrears, so for example, if your last day is January 7, any January commissions would be paid at the end of February. If you have any questions on this payment, please contact Swetha Akshyap, Sales Commissions Manager at `skashyap@gitlab.com`.

**What about your expense report?**

Before Access has been terminated:

Please create and submit your final expense report in Expensify (for employees), OR, please file any outstanding expenses with your final invoice (for contractors), so these can be reimbursed to you in a timely manner.

After access has been terminated: Please fill out your expenses on the [attached form](https://docs.google.com/spreadsheets/d/1SGYc7X5omrq24dR-Oao1EYq1gE0ksCUGT88_vUMLiV0/edit#gid=0) and send it via email to your Manager for approval. Attach your receipts to the email as well. You need to change the entity on the form to reflect the correct one that payment should be made under. Remember to indicate your bank details on the form where the money should be deposited. Once your Manager has reviewed and approved they will forward the same to AP@gitlab.com for processing.

# Benefits-related

**When will your benefits coverage end?**
If you are a contractor, this does not apply.
If you are employed through a PEO, please contact them directly.
If there are any issues that they cannot help with, please contact the Total Rewards team by emailing total-rewards@gitlab.com.
If you are enrolled in Medical Insurance as a team member of GitLab Ltd (UK), your benefits will end on the date of termination. You may be able to transfer your coverage to a personal plan and still have any existing medical conditions covered without the need for medical underwriting. Call AXA PPP Healthcare at 0800 028 2915 to get this process started.
For any other benefits, please reach out to the plan administrator or total-rewards@gitlab.com with any questions.
If you move, please email people-exp@gitlab.com so that the People Experience team can keep your address up to date in BambooHR.

**How will your stock options be managed?**
If you joined after 2015, you have 90 days to purchase your vested stock options. Those based in the US can exercise directly through Carta. Those based outside of the US will have to follow the instructions for a manual exercise. Instructions on “How to Exercise Your Stock Options” can be found [here](https://about.gitlab.com/handbook/stock-options/#how-to-exercise-your-stock-options). If you require assistance, or have any questions, please email stockadmin@gitlab.com. You can also reach Carta support by calling (650) 669-8381. A Carta support pin will be required. Information on how to obtain that pin can be found [here](https://support.carta.com/s/article/support-pin)

**What about my laptop?**
To better streamline the process please fill out [THIS](https://forms.gle/yVnhCrWwHjHWEoyz9) form. Your responses will be recorded and sent to Gitlab's IT team for review. Once your responses are reviewed IT Ops will reach out to discuss options pertaining to keeping/purchasing your GitLab laptop, and most importantly with directions for wiping the physical hard drive. Please be aware that IT Ops needs to wipe the hard drive (as per their instructions) and that you should not attempt to wipe your own hard drive. Please contact [it-help@gitlab.com](mailto:it-help@gitlab.com) for more information.
