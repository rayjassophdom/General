Subject: I-9 Completion Required - URGENT

Dear [[emp_first_name]],

Please note, a temporary access link has been generated for you to access our electronic I-9 system. The Form I-9 is a federal form used to verify the identity and work eligibility of those working in the United States. Please use the link below to complete your assigned task as soon as possible.

Thank you for your attention to this matter.

[[employee_link]]
