Subject: [Team Member Full Name] - Probation Period Completed 
To: [Team Member GitLab Email], cc `people-exp@domain.com`)

Dear [Team Member First Name],
 
Your appointment with the company was subject to a probationary period of [Probation Period Length]. Following regular reviews throughout your probation period, I am delighted to inform you that you have successfully completed this probationary period and your employment will continue with the company.
 
Your probationary period counts towards your continuous service with the organization. Your notice period is now one month, all other terms and conditions of your employment remain unchanged.
 
I would like to take the opportunity to congratulate you on your hard work to date, and thank you for your ongoing commitment to results at GitLab!

Yours sincerely,
[People Experience Associate]
