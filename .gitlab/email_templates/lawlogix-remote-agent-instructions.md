Subject: Remote Agent Instructions -I9

Remote Agent Instructions for Completion of Form I-9
 
[[company_name]] is an organization that hires employees at remote locations.  The Immigration Reform and Control Act (IRCA) requires all U.S. employers to verify the employment eligibility and identity of all employees hired to work in the United States after November 6, 1986 by requiring employers to complete I-9 Employment Eligibility Forms for all new hires.

The person presenting the I-9 form to you is a remote new hire for our company. Because it is not physically possible for [[employee_name]] to report to any of our offices to complete this required paperwork, we are asking for your assistance in inspecting the employee's original documents and completing the Form I-9 according to Department of Homeland Security guidelines.

[[company_name]] has adopted a paperless, web-based system for completing an electronic Form I-9 that meets with all federal government requirements. [[employee_name]] has already started this process by using the electronic system to complete and digitally sign Section 1 of the I-9. We are asking you to act as our agent for the sole purpose of reviewing original documents presented by [[employee_name]] and completing and digitally signing Section 2 of the Form I-9 by accessing our online system. 

Please use the link below to access the online system.

[[remote_hire_agt_login]]
 
The system provides on screen guidance and instructions on completing this form, as well as links to official government instructions for the I-9.

According to I-9 requirements, an employer or its agent must physically inspect the original documents (fax or photocopies are not accepted) and determine whether the documents appear to be genuine and relate to the employee presenting them. We are requesting that copies of the employee's documents be retained by either electronic upload (provided through the system) if possible, or returned along with a printout of the completed and signed Form I-9 to [[company_name]] via the contact information below. [Optional for Notaries]: In addition, please complete, sign and notarize the Notary Authorization form and return it with the Form I-9 and supporting documents to [[company_name]] at the address below.

[[contact_hr_email]]
 
If you have any questions, you may contact us at the phone number or email address below.

Thank you for agreeing to act as our agent for I-9 inspection and completion purposes. 

[[contact_hr_name]]
[[company_name]]
[[contact_hr_phone]]
[[contact_hr_email]]
