Subject: Remote Hire Employee Instructions


New Hire Form I-9 Completion Instructions

As you may know, [[company_name]] is required by federal law to have the entire Form I-9 completed and signed by your third day of employment in order to establish your eligibility to work in the United States. 

To make sure your I-9 is completed by the deadline, please follow the steps below:

1. Access the online I-9 system using the link and access information below to complete section 1 of the I-9 form.

[[remote_hire_emp_login]] 

1. Choose a designated agent to verify your I-9 document(s) in person. This person needs to be a US Citizen, and 18 and over. They cannot be a notary. The agent can be a family member, friend, former co-worker, neighbor, etc.

1. *IMPORTANT. Please complete this form: https://forms.gle/6yxpFHVvwQ2LBQBf6 which will let us know the name and email of your agent. We will then forward the remote agent instruction to them to compelete the I-9 process. 

Every afternoon, we check the form responses and your designated agent will receive instructions to complete Section 2 with you there in person so that they may verify your documents in person. Please do not meet up with the agent before receiving confirmation from them that they have received their instructions. Your agent will also need photos or scans of your documents to upload to the LawLogix portal when prompted. When the system asks the agent to enter their "Title", this is their current work title, i.e. "Senior Project Manager", etc..

If you have any questions regarding this process, please contact [[contact_hr_name]] by email at people-exp@gitlab.com as soon as possible. 

Thank you for your prompt assistance with this matter. 


[[contact_hr_name]] 
[[company_name]]
