Subject: [`COUNTRY`] Quote needed for Country Conversion  
(Remember to cc `countryconversions@domain.com`)

Hello `VENDOR REP`,

We require a quote for `COUNTRY` as we are gathering data from multiple vendors. I have attached a PDF including job titles and USD annual salary for our team members currently based in `COUNTRY`. Presently we pay salaries in `CURRENCY` in this country, can you advise if you can continue same. 

Please note that the PDF is encrypted and password protected; the password will follow in a separate email. 

Additionally, it would be great if you could please provide the infomation below. This will enable us to evalaute and ensure fast paced progression. 

*  Employee & Employer social cost breakdown
*  Any additional setup fees
*  Expenses reimbursement fees 
*  All Benefits available & associated costing (A list of the benefits we are currently offering can be found here (link to applicable Benefits section in the hanbook)).
    * Benefits we typically like to offer are:
        - Private Medical Insurance 
        - Private Life Insurance / Income Protection 
        - Pension / Provident Fund 
*  What is the mandatory Probation Period in this location? Can we remove this for current team members converting that have been with GitLab for longer than the duration of the probation period?

_Note: With respect to benefits, please clearly indicate which benefits are included in the PEO fees, and which benefits would carry an additional cost_ 

Please let us know if you need additional information from our end. 

Sincerely,
