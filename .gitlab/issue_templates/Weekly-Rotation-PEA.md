# This Template is used to track and allocate tasks within the People Experience Team. 

On a bi-weekly basis this template will be created, assigning the relevant People Experience Associates to the specific rotation. This will also be used to update as tasks are completed, handover of certain items and will be assigned to all Associates. 

# Rotations

## Verification of Employment and Probation Period

### Assigned: 

##### VOE 
- [ ] Respond to any Verification of Employment requests received for team members.

***ensure consent is received by the applicable team member before providing any personal information.***

##### [Triage Issues](https://about.gitlab.com/handbook/people-group/people-experience-team/#triage-people-group-issues) 

- [ ] Week 1 - Review and update labels within People issue tracker
- [ ] Week 2 - Review and update labels within People issue tracker

##### [Probation Emails](https://about.gitlab.com/handbook/people-group/people-experience-team/#probation-period-rotation)
- [ ] Update tracker of probation periods falling during this week
- [ ] Ensure the email is automatically being sent to manager

## Allocations 

### Assigned: 

##### Allocations
Update Onboarding Tracker

1. Week one:
    - [ ] Monday
    - [ ] Wednesday 
1. Week two:
    - [ ] Monday
    - [ ] Wednesday 

- [ ] Update Offboarding
- [ ] Update Career Mobility 

## Invites and Reports 

### Assigned: 

##### Anniversary 
- [ ] The Associate in this rotation will send the monthly anniversary gift emails on the last working day of the specific month (alternatively first of the following month) as per this [process](https://about.gitlab.com/handbook/people-group/people-experience-team/#anniversary-period-gift-queries).
- [ ] The Associate in this rotation will pulll the new hire [Printfection report](https://about.gitlab.com/handbook/people-group/people-experience-team/) on Friday's
   - [ ] Week 1
   - [ ] Week 2
   
##### Send Moo Invite
- [ ] Send [Moo Invites](https://about.gitlab.com/handbook/people-group/people-experience-team/) on Tuesday of the current week  
   - [ ] Week 1
   - [ ] Week 2

##### Send TaNEWki Call Invites
- [ ] If applicable, send invitation to new hires for the Ta'NEW'ki welcome call 2 weeks before their start date. 

##### Social Call Metrics
- [ ] First week of the Month create a new issue and [Pull Social Call Metrics](https://about.gitlab.com/handbook/people-group/people-experience-team/#pulling-social-call-metrics)

***Please also pull the Mental Health Social Call metrics to track each month***

##### Close out the issue 

 Close this issue and open next 

## Additional Projects

Please use this section to update with any additional project information 


/label ~"PEA Team" ~"Workflow::In Progress"
