# Adding or Updating Contract Issue 

**This issue is to be used when a contract needs to be updated or added** 

- Title the issue (Name of Entity/PEO)- Contract (Update/Add)

## Steps for Updating/Adding Contract 

- Add link(s) for the new contract `link` 



### People Operations @gl-peopleops
- [ ] Add contract to GitLab General - `Active Contract Templates`
- [ ] Update sharing settings to be `Anyone at GitLab` and `Viewer` (this will allow all team members to see the contract but not make changes to it)
- [ ] Update tokens on the contract - review in Greenhouse if all tokens are available 
- [ ] Tag Legal in the comments and request review and approval of the final contract 
- [ ] Move previous (if applicable)  contract to the `Deprecated Contracts & Docs` 
- [ ] Update Contract Page with new link for the contract 
- [ ] Ping Candidate Experience to upload to Greenhouse

### Legal `@emilyplotkin`
- [ ] Make People Operations Specialist (Ashley Jameson) the owner of the contract 
- [ ] Comment in the issue your approval once tokens have been added from Greenhouse

### Candidate Experience `@gitlab-com/gl-ces`
- [ ] Upload to Greenhouse 


/label ~"Workflow::Triage" ~"pops::p1"
