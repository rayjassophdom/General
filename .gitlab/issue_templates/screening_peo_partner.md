_This template should be used when we are starting a conversation with a possible PEO partner._

## Issue Creation Steps
* [ ] Appropriately title the issue according to the country being evaluated using the format `[PEO Vendor] Review Screening - PEO Vendor` 
* [ ] Please mark this issue as _*confidential*_ and assign to the Senior Manager, People Operations and cc `@gl-peopleops`

## Problem or Issue statement

The People Operations Specialist team currently works with PEO partners and issues have included:

* Delayed responses
* Sub-par support
* Inaccurate umbrella rules that do not apply to our team members
* Late payments
* Confusing and hidden pricing
* Long onboarding periods (2-3 weeks)
* Incomplete and corporate communication style
* Payroll issues

## Impact

The impact of these negative issues to the team members includes:

* Negative hiring and onboarding experience
* Negative issues pertaining to PTO and payroll
* Negative feeling towards GitLab that we choose to partner with these vendors

## Action Items

* [ ] Set up a time for a call with the vendor rep(s). Prior to the actual call, share an agenda containing all questions listed below so that they can be prepared. 
* [ ] Add the answers and notes to this issue and make a recommendation on how to proceed.

## Questions & Notes

**Tell us about your company history and current status?**

**How many countries do you support?**

**Which countries do you not support?**

**Do they use 3rd parties in most of these?**

**How do you support benefits? What type of benefits do you usually offer?**

**Onboarding timelines for converting contractors to be employees under a PEO?**

**Onboarding timeline for new hires hired directly under a PEO?** 

**What is your onboarding process like?**

**Payroll cut off dates? (We prefer past the 10th of each month and not before)** 

**Do you have remote clients? Can you talk about your knowledge of remote working?** 

**Pace / Scaling?**

**Some PEOs support entity conversions as a service (mainly to prevent termination fees) what is your vision on that?**

**Response SLA's?** 

**Pricing? (Onboard cost per user, and fees)**

**As an example, what is your solution in Portugal?**

## Proposal & Reasoning
