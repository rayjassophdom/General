_This template should be used when we are converting team members from contractors to a PEO, an existing PEO to a new PEO, or contractors to contractors through a PEO._

**Welcome** to your Country Conversion issue! We wanted to share a few things with you when you read and explore the conversion process with us. Kindly note a few things that happens during country conversions: 

- We do all the research live in this issue to ensure we are transparent in our process, dogfooding GitLab for this purpose. 
- We appreciate all feedback. Please consider talking to us directly or using the Q&A call in Phase 2 as a group discussion, before escalating to your manager/ VP or e-team member. Our team is highly supportive and understands change is hard, and we want to assure you that we are here to support you. We work through this process collaboratively and strive to get the best possible outcome.
- [We will be converting all GitLab BV and IT BV Contractor locations](https://about.gitlab.com/handbook/people-group/employment-solutions/#country-conversions) to ensure a long-term scalable employment solution. The actual conversion is not negotiable, but the way we convert might be! That is why any information you share with us is hugely appreciated. 
- This issue will be linked to a Total Rewards Issue and might be linked to a few other issues too. We collaborate across many teams to complete a Country Conversion.

**Changes to expect**: 
- If you are converting from a contractor status to a team member, your payment date will change to match GitLab's payroll timelines. This is usually at the end of the month.
- We have no control over income taxes, we, therefore, compare gross to gross when we review compensation. In doing so, we can ensure that each conversion is treated in a fair and consistent manner. GitLab cannot compensate for personal income tax deductions.  

**Issue Creation Steps**

* [ ] Appropriately title the issue according to the country being evaluated using the format `[COUNTRY NAME] Contractor Conversion (Conversion Type)` (I.E. Guatemala Contractor Conversion (Contractor to PEO))
* [ ] Please mark this issue as _*confidential*_ and assign to:
  * The Employment Solutions Partner
  * Director of Tax
* [ ] Add a due date (Effective date of conversion)

## Phase 1 : Initial Communication 

* [ ] Employment Solutions: Send [an initial email](https://gitlab.com/gitlab-com/people-group/people-operations/General/-/blob/master/.gitlab/email_templates/first_conversion_announcement.md) to all team members currently located in this country, cc their managers, skip level managers and `peoplepartners@gitlab.com` for awareness and link this issue so they may follow progress and provide feedback. 
* [ ] Employment Solutions: In the issue, ping all hiring managers of affected team members, and comment to let the hiring managers know that their direct report(s) is/are located in a country undergoing this process.
* [ ] Employment Solutions: Add the issue Link, hiring status (Can hire), Detail and Hiring Restrictions as paused in the [country hiring status yml](https://gitlab.com/-/ide/project/gitlab-com/people-group/peopleops-eng/compensation-calculator/edit/main/-/data/country_hiring_status.yml), which is now the SSoT over the previous [Country Conversion and Entity Creation Tracking Epic](https://gitlab.com/gitlab-com/people-group/people-operations/General/-/issues/367). These updates will show up on the [Country Hiring Status](https://comp-calculator.gitlab.net/hiring_status/index) page of the Compensation Calculator, which is now the SSoT and where all GitLab team members should go for current information.
* [ ] Employment Solutions: Notify the Recruiting team by pinging `@gl-recruiting` in the Country Conversion and Entity Creation Tracking issue.
* [ ] Employment Solutions: Make the *CES* team aware of this change by posting in people-group on slack. It is important that upcoming new hires are aware of this change.  

## Phase 2 : Research and Discovery

* [ ] Employment Solutions: Reach out to all country vendors to obtain quotes and benefits information.
* [ ] Employment Solutions: Open a [benefits review issue](https://gitlab.com/gitlab-com/people-group/total-rewards/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=#) to collaborate with the total rewards team on all things benefits related.
* [ ] Employment Solutions: Input all benefits data and quotes from potential partners into the benefits review issue and share the link in the Benefits section of this issue. Ping all affected team members. 
* [ ] Employment Solutions: Arrange a Q&A call. This session is for the team members to ask questions, but also for the International Expansion team to ask questions and explore what works and what does not work in this location. Invite the affected team members' managers as well.
* [ ] Employment Solutions: Input all data into the [Country Conversions sheet](https://docs.google.com/spreadsheets/d/1VUNv3qTAuDAsrXqY02mk05hik64UvckzjDDbyfuTdIo/edit#gid=1699067320) as it is received. 
* [ ] Employment Solutions: Once all quote and benefits information has been obtained from country vendors, ping the Total Rewards Manager in the country issue for a benefits review.<br>
_Note: Remember to include any previous benefit-related information that was shared by GitLab team members in this location_<br>

## Phase 3 : Decision-Making 

* [ ] Employment Solutions: Request approval from the Director of Tax via email providing an overview of options and the Country Conversion's team recommendation using the [email template](https://gitlab.com/gitlab-com/people-group/people-operations/General/-/blob/master/.gitlab/email_templates/country_conversion_recommendation.md).
* [ ] Director of Tax: Director of Tax will recommend decision to CFO, and then when replying to `countryconversions @gitlab.com` will cc the CFO on the final reply to document final approval. 
* [ ] Employment Solutions: If applicable, serve notice to current PEO, templates [here](https://gitlab.com/gitlab-com/people-group/people-operations/General/-/tree/master/.gitlab/email_templates).
* [ ] Employment Solutions: Save the email approval in the corresponding country folder located in the [Country Vendor Information](https://drive.google.com/drive/u/0/folders/14nhToiahmt5ANKoHGTbJztjaQ3jEVP6r) folder in Drive.
* [ ] Employment Solutions: Send an email to the selected country vendor to confirm we would like to move forward and cc `nonuspayroll@gitlab.com` and `countryconversions@gitlab.com`.
* [ ] Employment Solutions: Search the handbook and update any mention of the country undergoing the conversion so that it is marked "In Progress" so that any team member can be aware that a conversion is in progress.
* [ ] CES: Review if there are any new hires in the pipeline that need to be part of the conversion, or starting after the conversion date and notify the People Operations Team member that is handling the conversion.
* [ ] Employment Solutions: If due to the conversion, team members must be paid in a local currency other than USD, use the [Salary Conversions Template](https://docs.google.com/spreadsheets/d/1BcyTPwCU98syTgRciiDdTtod8gz5qkqW6rtkLTqfAfs/edit#gid=1532242709) to find the correct rounded compensation in local currency. Share the results with Total Rewards so that they may approve your calculations.
* [ ] Employment Solutions: Email the team member an overview of the approved total reward package using the following [template](https://docs.google.com/spreadsheets/d/15TiMn5QS9Wi7Wo0H2YxrOrydwulCcz5RFjHUgNh10GQ/edit#gid=0), ensure managers are CC'd & give an option for a follow up sync. Remember to password protect this information to remain compliant to GDPR

## Phase 4 : Notification and Pre-Launch

* [ ] Employment Solutions: Edit and send [team member conversion email](https://drive.google.com/drive/folders/105ZI49EwHHCtrwCJ5eqkHaPZh5ajal4b) to team members and cc managers, `total-rewards@gitlab.com`, `nonuspayroll@gitlab.com`, and `countryconversions@gitlab.com`. Check if the country vendor has a specific template that should be used for this conversion, due to specific requirements in the location, including documents that will be needed by the country vendor.
* [ ] Employment Solutions: Tag the Recruiting team `@gl-recruiting` in the country issue to confirm we have selected a country vendor and highlight the conversion date.
* [ ] Employment Solutions: Ping the [Director, Corporate Development](https://about.gitlab.com/company/team/#eliran.mesika) in the country issue to let them know that a location has been added or removed from the countries blocked from hiring list and link to the MR where it is updated.
* [ ] Employment Solutions: Introduce the CES Team (`ces@gitlab.com`) to the country vendor point of contact to clarify the hiring process for new hires moving forward.
* [ ] Employment Solutions: Notify the People Experience Team `@gl-people-exp` of this conversation in order to update the relevant onboarding entity specific information. Use the same opportunity to notify of any changes in addition to the entity/PEO change. 
* [ ] Employment Solutions: Arrange a kick-off call with all team members in this location to communicate that we are transitioning and the transition date we have in mind. Ensure that the selected country vendor is also present on this call (if possible) to answer any questions the team has. The flow of this call will normally consist of the vendor doing some brief introductions and a presentation overview, team members will then be able to ask any questions. <br> This should also be recorded and issue will be updated with the recording link, to ensure we remain asynchronous. 
**Important**: Team members should be given a minimum of one calendar month's notice _prior_ to the conversion.
* [ ] Employment Solutions: Complete and send the New Hire spreadsheets to country vendor, these should go to the onboarding contact and not the account manager. Confirm if they should be reviewed by GitLab before sending completed forms to the vendor.
* [ ] Employment Solutions: Review country vendor contracts and ask if we can implement language into the contract to support our [On-Call](https://about.gitlab.com/handbook/on-call/) policy to ensure compliance. Example language that can be adjusted per region is: `You will work an average of [NUMBER OF HOURS] a week, normal hours of work are between [TIME] and [TIME], [Mondays] to [Fridays] inclusive, with a lunch break of one hour. You may be required to work such additional hours as are necessary for the proper performance of your duties. If there are any provisions for paid overtime or time off in lieu specific to your role, these will be notified to you by your Manager. You will not be entitled to extra pay for working additional hours unless this has been previously agreed by your Manager or is required by law."`
* [ ] Employment Solutions: Share a draft of the agreement with the Director, Legal - Employment for additional feedback.
* [ ] Employment Solutions: Once approved by the Director, Legal - Employment, share a draft of the agreement with the team members for additional feedback, and escalate queries to Director, Legal - Employment as needed.

**Payroll & AP** 
  
* [ ] Employment Solutions: Work with payroll and the PEO to align on the payroll process.
* [ ] Employment Solutions: Ensure payroll are aware of benefits and the administration of such (this particularly important if we have cash allowances or statutory bonuses).
* [ ] Employment Solutions: Update the expense process in the [handbook](https://about.gitlab.com/handbook/finance/expenses/). Ping Courtney Cote on the issue as a new policy may need to be setup in expensify.
* [ ] Employment Solutions: Provide AP with a list of all team members that will need to be moved over to the new policy.

## Phase 5 : Launch

* [ ] Employment Solutions: Sync with country vendor on when the contracts should be sent to the team members so that the GitLab Mutual Termination document can be sent ~72 hours later. 
* [ ] Employment Solutions (for Entity conversion) or PEO (if PEO conversion): Contracts are sent for signatures, with signing order set. Send to the authorized signatory as listed in the [Entity Contract Signatories](https://about.gitlab.com/handbook/hiring/talent-acquisition-framework/ces-contract-processes/#entity-contract-signatories) table first, then to the converting team member. Be sure to send to the team member's personal email address (cc GitLab email address). If the specific country has rules in which the vendor cannot share the signed contract with GitLab, request permission for a line to be included in the PEO contracts or seek direct approval from the team member. If permission is not granted please note for PBP's that the PEO retains access to the contract.
* [ ] Employment Solutions: If the conversion is to a GitLab entity, save signed originals of the employment agreements in each team member's BambooHR profile, in the Contracts & Changes folder.
* [ ] Employment Solutions: Notify the team members that they will be receiving Mutual Termination Agreements using [the email template](https://gitlab.com/gitlab-com/people-group/people-operations/General/-/blob/master/.gitlab/email_templates/mutual_term_agreement.md).
* [ ] Employment Solutions: Prepare GitLab BV/IT BV Mutual Termination Agreements using our [template](https://drive.google.com/drive/search?q=template%20for%20gitlab%20bv/it%20bv%20mutual%20termination%20agreement) and stage in HelloSign (Send to the team members personal email address). Currently, the Senior Director, People Success is the GitLab signatory for this document. NOTE: If team members are C2C contractors, their Company must be listed on the same line as the team member's name.
* [ ] Employment Solutions: Get all benefits information for the location and save in the shared drive under the specific Country Conversions/country vendor folder/Employee and Contracts Folder.
* [ ] Employment Solutions: Check if the team members require an additional Q&A call with the country vendor and proceed to arrange it at their request.

## Phase 6 : Updating Systems and Documentation

**GitLab**

* [ ] Employment Solutions: Document the notice period in the [entity mapper](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/entity_mapper.yml) file.
* [ ] Employment Solutions: If required, update the [Currency Conversions](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/currency_conversions.yml) with the new currency and assign to the Total Rewards team to merge. The [exchange rates](https://about.gitlab.com/handbook/total-rewards/compensation/#exchange-rates) can be found in the Handbook and may also need to be updated. 
* [ ] Employment Solutions: Update the [Country Hiring Status](https://gitlab.com/-/ide/project/gitlab-com/people-group/peopleops-eng/compensation-calculator/edit/main/-/data/country_hiring_status.yml) file and update to "hiring: true" if setting was "hiring:false".
* [ ] Employment Solutions: Update the country status and information in the [Country Conversion and Entity Creation Tracking](https://gitlab.com/gitlab-com/people-group/people-operations/General/-/issues/367) issue.

**Greenhouse**

* [ ] Employment Solutions: Ping Recruiting Operations in this issue to update Greenhouse's `entity` choices to add a new "Entity" if applicable. 

**BambooHR**

* [ ] Employment Solutions: Request approval from Total Rewards to add a `Location` in the Job Information section of BambooHR. Use the same format as other locations (ex: CXC Russia, Global Upside - India, etc.). The new `Location` name must be approved by Total Rewards prior to adding to BambooHR. 
* [ ] Employment Solutions: Effective on the country conversion date, all team member's BambooHR profiles should be updated to reflect the employment location added above, as well as their new compensation, payroll type, currency conversion (if applicable), employment status (if applicable).
* [ ] Employment Solutions: Notify Total Rewards of all changes that have been updated in the team member's profiles so that they may audit. 
* [ ] Employment Solutions: Create a Time Off policy (if applicable) in BambooHR and add all team members to said policy with the correct policy start date.

**Handbook**

* [ ] Employment Solutions: Work with Total Rewards, Legal, and local counsel (if needed) to create a Time Off policy (if applicable). Document this policy in the [Statutory Vacation Requirements](https://about.gitlab.com/handbook/paid-time-off/#statutory-vacation-requirements) section of the Paid Time Off page on the handbook.
* [ ] Employment Solutions: Obtain the following information from each country-specific PEO regarding their offboarding process and document in `PBP Resources - Offboarding Documents and Templates - PEO Offboarding Processes`
- What is the offboarding process?
- Which parts of the offboarding process does GitLab manage?
- Which parts of the offboarding process does the PEO manage?
- What are performance management requirements?
- What are documentation requirements? 
- Are there any templates you can share with us? (Ex. separation agreement templates, offboarding templates). 
- What is the SLA for performance-related issues? (This needs to be less than 24 hours at least).

* [ ] Employment Solutions: Save a copy of the approved contract template to the [PEO Contract Template Folder](https://drive.google.com/drive/u/0/folders/1LoVmpr4tAoMhvDqYDPa7EUiHTxJPeszI) 
* [ ] Employment Solutions: Update the [entity mapper](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/entity_mapper.yml) which feeds to the [Probation Period](https://about.gitlab.com/handbook/people-group/contracts-probation-periods/#probation-period) handbook section, and update any content related to that process if applicable.  
* [ ] Employment Solutions: Email `peoplepartners@gitlab.com` to confirm that the conversion is complete. 

**Contract Works**

* [ ] Employment Solutions: Upload PEO exhibit.

**Important**: If any team member has concerns about the conversion, we recommend waiting until you receive your contract, which will show full compensation information, all benefits and a more holistic view of what the change holds. We provide a full two weeks to review contracts and signing. If you want to raise a concern at that point, email: countryconversions@gitlab and cc your manager and [People Business Partner](https://about.gitlab.com/handbook/people-group/#people-business-partner-alignment-to-division).

## Benefits Review

Your Benefits Review issue is located here (insert link).

* [ ] Employment Solutions: Provide the following [letter](https://docs.google.com/document/d/1xpq7JM5WuuFrfIRBo1HWAhbGwg3jYfm_IKfIRPKOPz0/edit) to our team members so that they are aware that their GitLab associated benefits will continue. 
