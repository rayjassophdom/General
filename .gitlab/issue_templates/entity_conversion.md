_This template should be used when we are converting team members from contractors / PEO to an entity. 

**Welcome** to your Country Conversion issue! We wanted to share a few things with you when you read and explore the conversion process with us. Kindly note a few things that happens during country conversions: 

- We do all the research live in this issue to ensure we are transparent in our process, dogfooding GitLab for this purpose. 
- We appreciate all feedback. Please consider talking to us directly or using the Q&A call in Phase 2 as a group discussion, before escalating to your manager/ VP or e-team member. Our team is highly supportive and understands change is hard, and we want to assure you that we are here to support you. We work through this process collaboratively and strive to get the best possible outcome.
- [We will be converting all GitLab BV and IT BV Contractor locations](https://about.gitlab.com/handbook/people-group/contracts-and-international-expansion/#country-conversions) to ensure we are compliant in every country in the world. The actual conversion is not negotiable, but the way we convert might be! That is why any information you share with us is hugely appreciated. 
- This issue will be linked to a Total Rewards Issue and might be linked to a few other issues too. We collaborate across many teams to complete a Country Conversion.

**Changes to expect**: 
- If you are converting from a contractor status to a team member, your payment date will change to match GitLab's payroll timelines. This is usually at the end of the month.
- We have no control over income taxes, we, therefore, compare gross to gross when we review compensation. In doing so, we can ensure that each conversion is treated in a fair and consistent manner. GitLab cannot compensate for personal income tax deductions.  

**Issue Creation Steps**

* [ ] Appropriately title the issue according to the country being evaluated using the format `[COUNTRY NAME] Contractor Conversion (Conversion Type)` (I.E. Guatemala Contractor Conversion (Contractor to Entity))
* [ ] Please mark this issue as _*confidential*_ and assign to:
  * The assigned People Operations Specialist (found in row 2 of the `Summary & Recs` tab in the [Country Conversions sheet](https://docs.google.com/spreadsheets/d/1VUNv3qTAuDAsrXqY02mk05hik64UvckzjDDbyfuTdIo/edit#gid=1699067320))
  * Director of Tax
* [ ] Add a due date (Effective date of conversion)

## Urgency  

What is the level of priority & urgency for this conversion as indicated in the Risk Matrix?
* [ ] `High-1`
* [ ] `High-2`

## Phase 1 : Initial Communication 

* [ ] People Ops: Send [an initial email](https://gitlab.com/gitlab-com/people-group/General/-/blob/master/.gitlab/issue_templates/cc_emailtemplate_generic_announcement.md) to all team members currently located in this country, cc their managers, skip level managers and `peoplepartners@gitlab.com` for awareness and link this issue so they may follow progress and provide feedback. 
* [ ] People Ops: In the issue, ping all hiring managers of affected team members, and comment to let the hiring managers know that their direct report(s) is/are located in a country undergoing this process.
* [ ] People Ops: Update the conversion status and link to the issue on the [EPIC](https://gitlab.com/gitlab-com/people-group/General/-/issues/367) - Country Conversion and Entity Creation Tracking. Add the statement **HIRING PAUSED** to the Status if we do not currently have a support PEO. 
* [ ] People Ops: Notify the Recruiting team by pinging `@gl-recruiting` in the Country Conversion and Entity Creation Tracking issue.
* [ ] People Ops: Make the *CES* (gitlab-com/gl-ces) team aware of this change by posting in people-group on slack. It is important that upcoming new hires are aware of this change.  

## Phase 2 : Research and Discovery

* [ ] People Ops: Work with Director of Tax, Payroll & Legal to define the information surrounding the entity.
* [ ] People Ops: Open a [benefits review issue](https://gitlab.com/gitlab-com/people-group/total-rewards/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=#) to collaborate with the total rewards team on all things benefits related.
* [ ] People Ops: Ping all affected team members. 
* [ ] People Ops: Arrange a Q&A call. This session is for the team members to ask questions, but also for the International Expansion team to ask questions and explore what works and what does not work in this location. Invite the affected team members' managers as well.

## Phase 3 : Decision-Making 

* [ ] People Ops: If applicable, serve notice to current PEO, templates [here](https://gitlab.com/gitlab-com/people-group/General/-/tree/master/.gitlab/email_templates).
* [ ] People Ops: Save the email approval in the in the corresponding country folder located in the [Country Vendor Information](https://drive.google.com/drive/u/0/folders/14nhToiahmt5ANKoHGTbJztjaQ3jEVP6r) folder in Drive.
* [ ] People Ops: Search the handbook and update any mention of the country undergoing the conversion so that it is marked "In Progress" so that any team member can be aware that a conversion is in progress.
* [ ] CES: Review if there are any new hires in the pipeline that needs to be part of the conversion, or starting after the conversion date and notify the People Operations Team member that is handling the conversion.
* [ ] People Ops: If due to the conversion, team members must be paid in a local currency other than USD, use the [Salary Conversions Template](https://drive.google.com/drive/search?q=%22salary%20conversions%20template%22) to find the correct rounded compensation in local currency. Share the results with Total Rewards so that they may approve your calculations.
* [ ] People Ops: Email the team member an overview of the approved total reward package using the following [template](https://docs.google.com/spreadsheets/d/15TiMn5QS9Wi7Wo0H2YxrOrydwulCcz5RFjHUgNh10GQ/edit#gid=0), ensure managers are CC'd & give an option for a follow up sync. Note: This will only be sent if there is a change to current compensation.  

## Phase 4 : Notification and Pre-Launch

* [ ] People Ops: Edit and send [team member conversion email](https://drive.google.com/drive/folders/105ZI49EwHHCtrwCJ5eqkHaPZh5ajal4b) to team members and cc managers, `total-rewards@gitlab.com`, `nonuspayroll@gitlab.com`, and `countryconversions@gitlab.com`. Check if the country vendor has a specific template that should be used for this conversion, due to specific requirements in the location, including documents that will be needed by the country vendor.
* [ ] People Ops: Tag the Recruiting team `@gl-recruiting` in the country issue to confirm we have selected a country vendor and highlight the conversion date.
* [ ] People Ops: Work with the CES Team (`ajones@gitlab.com` and `csapolu@gitlab.com`) to clarify the hiring process for new hires moving forward.
* [ ] People Ops: Notify the People Experience Team `@gl-people-exp` of this conversation in order to update the relevant onboarding entity specific information. Use the same opportunity to notify of any changes in addition to the entity/PEO change. 
* [ ] People Ops: Arrange a kick-off call with all team members in this location to communicate that we are transitioning and the transition date we have in mind. Ensure that the selected country vendor is also present on this call (if possible) to answer any questions the team has. The flow of this call will normally consist of the vendor doing some brief introductions and a presentation overview, team members will then be able to ask any questions. <br> This should also be recorded and issue will be updated with the recording link, to ensure we remain asynchronous. 
**Important**: Team members should be given a minimum of one calendar month's notice _prior_ to the conversion (Issue open date kicks off notice).
* [ ] People Ops/Legal: Review country contracts.
* [ ] People Ops: Share a draft of the agreement with team members for additional feedback allow up to two weeks for feedback and up to one week for signing.
* [ ] People Ops: Personalise and stage contracts for signing.

**Payroll & AP** 
  
* [ ] People Ops: Work with payroll to align on the payroll process.
* [ ] People Ops: Ensure payroll are aware of benefits and the administration of such (this particularly important if we have cash allowances or statutory bonus')
* [ ] People Ops: Update the expense process in the [handbook](https://about.gitlab.com/handbook/finance/expenses/). Ping Courtney Cote on the issue as a new policy may need to be setup in expensify.
* [ ] People Ops: Provide AP with a list of all team members that will need to be moved over to the new policy


## Phase 4 : Launch

* [ ] People Ops: Notify the team members that they will be receiving Mutual Termination Agreements using [the email template](https://gitlab.com/gitlab-com/people-group/General/-/blob/master/.gitlab/email_templates/mutual_term_agreement.md).
* [ ] People Ops: If applicable, prepare GitLab BV/IT BV Mutual Termination Agreements using our [template](https://drive.google.com/drive/search?q=template%20for%20gitlab%20bv/it%20bv%20mutual%20termination%20agreement) and stage in HelloSign. Currently, the Senior Director, People Success is the GitLab signatory for this document. NOTE: If team members are C2C contractors, their Company must be listed on the same line as the team member's name.
* [ ] People Ops: Save signed originals of the employment agreements in each team member's BambooHR profile, in the Contracts & Changes folder.
* [ ] People Ops: Get all benefits information for the location and save in the shared drive under the specific Country Conversions/country vendor folder/Employee and Contracts Folder.
* [ ] People Ops: Check if the team members require an additional Q&A call and proceed to arrange it at their request.

## Phase 5 : Updating Systems and Documentation

**GitLab**

* [ ] People Ops: Update the compensation calculator by modifying the [Location factor](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/location_factors.yml) and the [Contract Factor](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/contract_factors.yml) then assign to the Total Rewards team to merge. _Note: The compensation calculator needs to be updated regardless of whether or not the factor changed (I.E. 1 to 1.17 or vice versa) to ensure that the country is listed as open for hiring on the compensation calculator page. Please reference the `factor` link above for examples._
* [ ] People Ops: Document the notice period in the [entity mapper](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/entity_mapper.yml) file.
* [ ] People Ops: Update the [Country Hiring Status](country_hiring_status.yml) file and update to "hiring: true" if setting was "hiring:false".
* [ ] People Ops: Update the country status and information in the [Country Conversion and Entity Creation Tracking](https://gitlab.com/gitlab-com/people-group/General/-/issues/367) issue.

**Greenhouse**

* [ ] People Ops: Ping Recruiting Operations in this issue to update Greenhouse's `entity` choices to add a new "Entity" if applicable. 

**BambooHR**

* [ ] People Ops: Request approval from Total Rewards to add a `Location` in the Job Information section of BambooHR. Use the same format as other locations (ex: CXC Russia, Global Upside - India, GitLab Ireland Ltd etc.). The new `Location` name must be approved by Total Rewards prior to adding to BambooHR. 
* [ ] People Ops: Effective on the country conversion date, all team member's BambooHR profiles should be updated to reflect the employment location added above, as well as their new compensation, payroll type, currency conversion (if applicable), employment status (if applicable).
* [ ] People Ops: Notify Total Rewards of all changes that have been updated in the team member's profiles so that they may audit. 
* [ ] People Ops: Create a Time Off policy (if applicable) in BambooHR and add all team members to said policy with the correct policy start date.

**Handbook**

* [ ] People Ops: Work with Total Rewards, Legal, and local counsel (if needed) to create a Time Off policy (if applicable). Document this policy in the [Statutory Vacation Requirements](https://about.gitlab.com/handbook/paid-time-off/#statutory-vacation-requirements) section of the Paid Time Off page on the handbook.
 [ ] People Ops: Collaborate with the GitLab Legal Team to obtain the following information from local counsel regarding the offboarding process and document in the [Process for Team Members Outside of the US](https://about.gitlab.com/handbook/people-group/offboarding/#process-for-team-members-outside-of-the-us) section of the Offboarding handbook page.

- What is the offboarding process?
- Which parts of the offboarding process does GitLab manage?
- Which parts of the offboading process does the PEO manage?
- What are performance management requirements?
- What are documentation requirements? 
- Are there any templates you can share with us? (Ex. separation agreement templates, offboarding templates). 
- What is the SLA for performance-related issues? (This needs to be less than 24 hours at least).

* [ ] People Ops: Email `peoplepartners@gitlab.com` to confirm that the conversion is complete. 

**Important**: If any team member has concerns about the conversion, we recommend waiting until you receive your total reward overview, which will show full compensation information, all benefits and a more holistic view of what the change holds. We provide a full two weeks to review contracts and signing. If you want to raise a concern at that point, email: countryconversions@gitlab and cc your manager and [People Business Partner](/handbook/people-group/#people-business-partner-alignment-to-division).

## Benefits Review

Your Benefits Review issue is located here (insert link).
