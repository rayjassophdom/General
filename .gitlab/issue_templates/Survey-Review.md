# Culture Amp Survey Review 
This template is used to request assistance with the survey process within CultureAmp. 

- If this is your first survey it is suggested to take the [Lauch Your Survey Course](https://www.cultureamptraining.com/launch-your-survey). 

## Timeline 
- When do you want this survey to go out? 

## Add any related issues or handbook pages 
1. `issue`
1. `handbook page`

## Related Previous Surveys
Add the names of any previous surveys you have sent out in Culture Amp that are related to this survey. 

### Concerns - Previous Survey
- Did you run into any issues with the previous survey? This will help identify what settings that might need to be adjusted to changed. 

## Configure 
### Configure survey
#### Settings
- Review if the survey is Attributed or Unattributed? 
   - If **unattributed** demographics will not be pulled from BambooHR, all demographics must be asked within survey.
- [ ] Audit
- Notes? 
#### Questions
- [ ] Audit
- Notes? 
#### Demographics
- Special requests for Demographics? (Sub-department?)
- [ ] Audit
- Notes? 
#### Participants
- Do they want the whole company or do they want to exlude new hire after a certain date? 
- [ ] Audit
- Notes? 
#### Reporting
- Review reporting group minimum, should be five for both. 
- [ ] Audit
- Notes? 
#### Communications
- Check the email address and email name. 
- [ ] Audit
- Notes? 
#### Launch plan
- [ ] Audit
- Notes? 
#### Sharing
- [ ] Audit
- Notes? 
### Settings
#### Administrators
- [ ] Audit
- Notes? 
 
#### Comments
- [ ] Audit
- Notes? 
#### Trend
- [ ] Audit
- Notes? 

- Make sure an index factor has been select if the trend feature is utilized
#### Comparisons
- [ ] Audit
- Notes? 
#### Inspirations
- [ ] Audit
- Notes? 

/label ~"Workflow::Triage" 
