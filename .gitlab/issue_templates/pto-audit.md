# PTO-Audit 
This template is used as a checklist and documentation of the audits completed by the People Operations Team. 

1. [ ] Pull a `Event Details` report from PTO by Roots 
1. [ ] Select the dates since the last audit 

**Family and Friends Day** 

-  Review how many [Family and Friends days](https://about.gitlab.com/company/family-and-friends-day/) that have occured during this audit period. 
-  Sort out anyone who has taken more than the above number. Correct the non-F&F days to the appropriate OOO categories.
-  Communicate the difference with the manager and team member. 
-  Adjust excess days to Vacation Days. Instructions [here](https://about.gitlab.com/handbook/paid-time-off/#instructions-for-people-ops-and-total-rewards-to-update-pto-by-roots-events). 



**Non-PTO Days** 
- Filter the report by Request Type and select the following types 
   - Conference
   - Bereavement
   - Mandatory Civilian Service
   - CEO Shadow Program
   - Customer Visit 
   - Jury Duty
- Review the Message and Number of Days 
- Reach out to the team member and their manager if anything should be recatergorized 


