This issue template is to be completed by a team member if they wish to seek an exception to our country hiring guidelines for the purposes of a relocation. At present we are focussing our hiring in countries where we have [entities](https://about.gitlab.com/handbook/people-group/employment-solutions/#gitlab-entities) & [PEO's](https://about.gitlab.com/handbook/people-group/employment-solutions/#peo-professional-employer-organization-employer-of-record-and-not-a-gitlab-entity). Relocating outside of a location with a PEO or Entity is not allowed without an approved exception.

***Team Member to complete below***

* [ ] Mark issue confidential
* [ ] Name: (Text)
* [ ] Current Location: (Text)

**What country are you looking to relocate to?**

* [ ] (Text)

**What are the reasons for this relocation?**

* [ ] (Text)

**Has this relocation been approved by your manager?**

* [ ] Yes
* [ ] No
* [ ] Tag manager: @ManagerName


**Has this relocation been approved by your People Business Partner?**

* [ ] Yes
* [ ] No
* [ ] Tag PBP: @PeopleBusinessPartner


**Do you have a timeframe in mind for relocating?**

* [ ] (Text)

***Employment Solutions to complete below***

**Exception Type**

* [ ] Relocation to a location where we currently have no team members
* [ ] Relocation to a location that is blocked for hiring with contractor team members
* [ ] Relocation to a location that is blocked for hiring with a PEO

**Requested location status**

* [ ] (Text)

**Associated Concerns / Barriers to entry**

* [ ] (Text)

**Recommendation**

* [ ] (Text)

***E/C Level Approval***

* [ ] Employment Solutions: Ping E/C level team member for overview / approval
