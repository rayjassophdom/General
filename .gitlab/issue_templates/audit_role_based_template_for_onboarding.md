Problem or Issue statement

Our team is working on moving all Role Based tasks lists into specific files that will be automatically added to the onboarding issue if necessary. In the current onboarding issue, we have the following role based task listed for `Insert Department`.

`Insert Team Department`

`Insert Role Specific Tasks that are currently listed in the relevant files`

What we need to move forward:

- [ ] Is there a specific job title, team, or department that should be receiving these tasks?
- [ ] Are there any additional tasks that need to be added?